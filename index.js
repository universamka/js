const API_URL = 'https://api.github.com/';
const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');
const fightersDetailsMap = new Map();

function callApi(endpoind, method) {
    const url = API_URL + endpoind
    const options = {
      method
    };
  
    return fetch(url, options)
      .then(response => 
        response.ok 
          ? response.json() 
          : Promise.reject(Error('Failed to load'))
      )
      .catch(error => { throw error });
}

class FighterService {
    async getFighters() {
      try {
        const endpoint = 'repos/sahanr/street-fighter/contents/fighters.json';
        const apiResult = await callApi(endpoint, 'GET');        
     
        //console.log(JSON.parse(atob(apiResult.content)));
        return JSON.parse(atob(apiResult.content));

      } catch (error) {
        throw error;
      }
    }

    // Function to get the details of the fighter by ID.
    async getFighterDetails(id) {
      try {
        const endpoint = 'repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/details/fighter/'+ id + '.json';
        const apiResult = await callApi(endpoint, 'GET');      
     
        return JSON.parse(atob(apiResult.content));
      } catch (error) {
        throw error;
      }
    }


}
  
const fighterService = new FighterService();



class View {
    element;
  
    createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
      return element;
    }
}

class FighterView extends View {
    constructor(fighter, handleClick) {
      super();
  
      this.createFighter(fighter, handleClick);
    }
  
    createFighter(fighter, handleClick) {
      const { name, source } = fighter;
      const nameElement = this.createName(name);
      const imageElement = this.createImage(source);
  
      this.element = this.createElement({ tagName: 'div', className: 'fighter' });
      this.element.append(imageElement, nameElement);
      this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }
  
    createName(name) {
      const nameElement = this.createElement({ tagName: 'span', className: 'name' });
      nameElement.innerText = name;
  
      return nameElement;
    }
  
    createImage(source) {
      const attributes = { src: source };
      const imgElement = this.createElement({
        tagName: 'img',
        className: 'fighter-image',
        attributes
      });
  
      return imgElement;
    }
  }
  
  class FightersView extends View {
    constructor(fighters) {
      super();

      this.handleClick = this.handleFighterClick.bind(this);
      this.createFighters(fighters);
    }
  
    fightersDetailsMap = new Map();
  
    createFighters(fighters) {
      const fighterElements = fighters.map(fighter => {
        const fighterView = new FighterView(fighter, this.handleClick);
        return fighterView.element;
      });
  
      this.element = this.createElement({ tagName: 'div', className: 'fighters' });
      this.element.append(...fighterElements);
    }
  
    handleFighterClick(event, fighter) {
      this.fightersDetailsMap.set(fighter._id, fighter);
      console.log('clicked')
    }
  }

  class App {
    constructor() {
      this.startApp();
    }
  
    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');
  
    async startApp() {
      try {
        App.loadingElement.style.visibility = 'visible';
        
        const fighters = await fighterService.getFighters();
        const fightersView = new FightersView(fighters);
        const fightersElement = fightersView.element;
  
        App.rootElement.appendChild(fightersElement);

        // Show the details of the fighters.
        const fighterDetails = new Fighters();
        fighterDetails.showFightersDetails();

      } catch (error) {
        console.warn(error);
        App.rootElement.innerText = 'Failed to load data';
      } finally {
        App.loadingElement.style.visibility = 'hidden';
      }
    }
  }

  
  
  // Class to save detaiks about the fighter.
  class Fighter{
    constructor(id, name, health, attack, defense) {           
      this.id = id;
      this.name = name;
      this.health = health;
      this.attack = attack;
      this.defense = defense;
    }

    getFighterInfo(){
      return new Map([
        ['id',  this.id],
        ['name', this.name],
        ['health', this.health],
        ['attack', this.attack],
        ['defense', this.defense]
      ]);
    }

    getCriticalHitChance(){
        let criticalHitChance =  Math.random() * (2 - 1) + 1;
        return criticalHitChance;
        
    }

    getDodgeChance(){
        let dodgeChance =  Math.random() * (2 - 1) + 1;
        return dodgeChance;
    }

    getHitPower(){
      return +(this.attack * this.getCriticalHitChance()).toFixed(3);
              
    }

    getBlockPower(){
       return  +(this.defense * this.getDodgeChance()).toFixed(3);
       
    }    
  }  

  class Fighters{ 
    async showFightersDetails() {    
      try {
        for(let i = 1; i < 7; i++){
          let fighterDetails = await fighterService.getFighterDetails(i);
          let fighter = new Fighter(fighterDetails._id, fighterDetails.name, fighterDetails.health, fighterDetails.attack, fighterDetails.defense); 
          console.log(fighter.getFighterInfo());
          console.log("Hit power: " + fighter.getHitPower());
          console.log("Block power: " + fighter.getBlockPower());
        }          
      } catch (error) {
        console.log(error);
      }                     
    }
  } 

 

  new App();
  
  
